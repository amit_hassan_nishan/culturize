import 'package:culturize/navigation_drawer.dart';
import 'package:flutter/material.dart';

class AboutUs extends StatefulWidget {
  @override
  _AboutUsState createState() => _AboutUsState();
}

class _AboutUsState extends State<AboutUs> {
  List<String> notes = [
    "Mobile   \n 01xxxxxxxxx",
    "Offlilne Stores \n Dhaka \n Chattogram \n Jashore",
    "Our Address \n Sonarpur College Gate, Sonapur, Sadar, Noakhali, Chattogram, Bangladesh",
    "Demo ListView simplenote app",
    "Fixing app color",
    "Create new note screen",
    "Persist notes data",
    "Add screen transition animation",
    "Something long Something long Something long Something long Something long Something long",
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: new Center(child: Text(
            'About Us',
            style: TextStyle(fontSize:  20.0, fontWeight: FontWeight.bold),),),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: (){
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => NavigationDrawerDemo()));
          },

        ),
      ),
      body: Container(
        color: Colors.white10,
        padding: EdgeInsets.all(16.0),
        child: HomeScreen(notes),
      ),
    );
  }
}

class HomeScreen extends StatelessWidget {
  final List<String> notes;

  HomeScreen(this.notes);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: notes.length,
      itemBuilder: (context, pos) {
        return Padding(
            padding: EdgeInsets.only(bottom: 16.0),
            child: Card(
              color: Colors.white,
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 24.0, horizontal: 16.0),
                child: Text(notes[pos], style: TextStyle(
                  fontSize: 18.0,
                  height: 1.6,
                ),),
              ),
            )
        );

      },
    );
  }
}
